//
//  ViewController.swift
//  LocalNotifications
//
//  Created by Jose Gonzalez on 10/1/18.
//  Copyright © 2018 Jose Gonzalez. All rights reserved.
//

import UIKit
import UserNotifications




class ViewController: UIViewController, UNUserNotificationCenterDelegate {

    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var resultLabel: UILabel!
    
    var notificationMessage = "El texto es: "
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //Para recuperar
        // 1. Instanciar UserDefaults
        let defaults = UserDefaults.standard
        
        // 2. Acceder el valor por medio del KEY
        
       resultLabel.text = defaults.object(forKey: "label") as? String
        
        UNUserNotificationCenter.current().delegate = self
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert])
        {
            (granted,error) in
        }
        
    }

    @IBAction func guardarButton(_ sender: Any) {
        
        resultLabel.text = nameTextField.text
        
        // Para guardar
        // 1. instanciar UserDefaults
        let defaults = UserDefaults.standard
        // 2. Guardar variables en defaults
        // Int, Boolean, Float, Double, Object!
        
        defaults.set(nameTextField.text, forKey: "label")
        
        sendNotification()
        
    }
    
    func sendNotification(){
        
          notificationMessage += resultLabel.text!
        
        // 1. Authorization request (está em DidLoad)
        // 2. Crear contenido de la Notification
        
        let content = UNMutableNotificationContent()
        content.title = "Notification Title"
        content.subtitle = "Notification Subtitle"
        content.body = notificationMessage
        
        //2.1 Crear acciones
    
        let repeatAction = UNNotificationAction(identifier: "repeat", title: "Repetir", options: [])
        let changeMessage = UNTextInputNotificationAction(identifier: "change", title: "Cambiar mensaje", options: [])

        
        // 2.2 Agregar acciones a un UNNotificationCategory
        
        let category = UNNotificationCategory(identifier: "actionsCat", actions: [repeatAction,changeMessage], intentIdentifiers: [], options: [])
        
        // 2.3 agregar el categoryIdentifier al content
        
        content.categoryIdentifier = "actionsCat"
        
        // 2.4 Agregar category al UNUserNotificationCenter
        
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
        
        
        // 3.Definir Trigger
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 15, repeats: false)
        
        // 4. Definir un identifier para la notificacion
        
        let identifier = "Notification"
        
        // 5. Crear un Request
        
        let notificationReques = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        // 6. Añadir el request al UNMutableNotificationCenter
        
        UNUserNotificationCenter.current().add(notificationReques) {
            
            (error) in
        }
        
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        switch response.actionIdentifier {
        case "repeat":
            self.sendNotification()
            
            break
        case "change":
            let txtResponse = response as! UNTextInputNotificationResponse
            notificationMessage += txtResponse.userText
            self.sendNotification()
            completionHandler()
            break
        default:
            break
        }
        
        
        print(response.actionIdentifier)
        completionHandler()
        
        

    }
    
    
    

}

